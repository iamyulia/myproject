﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyProject.Models.AccountViewModels
{
    public class EditViewModel
    { 
        public string Id { get; set; }

        public string Username { get; set; }

        public string AboutUser { get; set; }

        public string Image { get; set; }
    }
}
