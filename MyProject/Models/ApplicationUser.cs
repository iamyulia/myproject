﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace MyProject.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public bool UserIsLocked { get; set; } = false;
        
        public List<Note> Notes { get; set; }

        public List<Comment> Comments { get; set; }

        public List<Like> Likes { get; set; }

        public string AboutUser { get; set; }

        public string Image { get; set; }

    }
}
