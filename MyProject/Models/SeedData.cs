﻿using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using ItransitionFinalProject.Data;

namespace ItransitionFinalProject.Models
{
    public static class SeedData
    {
        public static void EnsurePopulated(IApplicationBuilder app)
        {
            FinalProjectDBContext context = app.ApplicationServices
                .GetRequiredService<FinalProjectDBContext>();
            context.Database.Migrate();
            if (!context.Notes.Any())
            {
                context.Notes.AddRange(
                    new Note
                    {
                        Name = "note 1",
                        Description = "Apple is fruit",
                        NumberSpecialty = "11 01"
                    },
                    new Note
                    {
                        Name = "note 2",
                        Description = "Apple is fruit",
                        NumberSpecialty = "11 01"
                    },
                    new Note
                    {
                        Name = "note 2",
                        Description = "Apple is fruit",
                        NumberSpecialty = "11 01"
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
