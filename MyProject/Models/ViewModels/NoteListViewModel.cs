﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyProject.Models;

namespace MyProject.Models.ViewModels
{
    public class NoteListViewModel
    {
        public IEnumerable<Note> Notes { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}
