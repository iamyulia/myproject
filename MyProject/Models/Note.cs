﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace MyProject.Models
{
    public class Note
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Пожалуйста, введите название конспекта")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Пожалуйста, добавьте описание конспекта")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Пожалуйста, добавьте содержание конспекта")]
        public string Text { get; set; }

        [Required(ErrorMessage = "Пожалуйста, введите номер специальности")]
        [Range(1, 12, ErrorMessage = "Неверный номер специальности")]
        public string NumberSpecialty { get; set; }

        public DateTime Date { get; set; }

        public string Image { get; set; }

        public string ApplicationUserId { get; set; }

        public ApplicationUser ApplicationUser { get; set; }

        public List<Comment> Comments { get; set; }

    }
}
