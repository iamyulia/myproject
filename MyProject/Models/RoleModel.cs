﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyProject.Models
{
    public class RoleModel
    {
        public IEnumerable<ApplicationUser> Admin { get; set; }
        public IEnumerable<ApplicationUser> NotAdmin { get; set; }
    }
}
