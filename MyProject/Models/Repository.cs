﻿using MyProject.Data;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;

namespace MyProject.Models
{
    public class Repository: IRepository
    {
        UserManager<ApplicationUser> userManager;

        private FinalProjectDBContext context;

        public Repository(FinalProjectDBContext ctx, UserManager<ApplicationUser> userManager)
        {
            context = ctx;
            this.userManager = userManager;
        }
        public IQueryable<Note> Notes => context.Notes;

        public IQueryable<Comment> Comments => context.Comments;

        public IQueryable<Like> Likes => context.Likes;

        public void AddNote(Note note)
        {
            context.Notes.Add(note);
            context.SaveChanges();
        }

        public void SaveNote(Note note)
        {
            Note dbEntry = context.Notes
            .FirstOrDefault(p => p.Id == note.Id);
            if (dbEntry != null)
            {
                dbEntry.Name = note.Name;
                dbEntry.Text = note.Text;
                dbEntry.Description = note.Description;
                dbEntry.NumberSpecialty = note.NumberSpecialty;
                    
                dbEntry.Date = DateTime.Now.Date;
                if (!string.IsNullOrEmpty(note.Image))
                {
                    dbEntry.Image = note.Image;
                }
            }
            context.SaveChanges();
        }

        public void DeleteNote(Note note)
        {
            context.Notes.Remove(note);
            context.SaveChanges();
        }

        public void DeleteComment(int commentId)
        {
            var comment = context.Comments.Single(c => c.Id == commentId);
            context.Comments.Remove(comment);
            context.SaveChanges();
        }

        public void AddComment(Comment comment)
        {
            context.Comments.Add(comment);
            context.SaveChanges();
        }

        public void DeleteCommentsOfUser(string userId)
        {
            var commentsToDelete = context.Comments.Where(comment => comment.ApplicationUser.Id == userId);

            context.Comments.RemoveRange(commentsToDelete);

        }

        public void AddLike(Like like)
        {
            context.Likes.Add(like);
            context.SaveChanges();
        }

        public void DeleteLike(Like like)
        {
            context.Likes.Remove(like);
            context.SaveChanges();
        }
    }
}
