﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyProject.Models
{
    public interface IRepository
    {
        IQueryable<Note> Notes { get; }

        IQueryable<Comment> Comments { get; }
        
        IQueryable<Like> Likes { get; }

        void AddNote(Note note);

        void DeleteNote(Note note);

        void SaveNote(Note note);

        void AddComment(Comment comment);

        void DeleteComment(int commentId);

        void DeleteCommentsOfUser(string userId);

        void AddLike(Like like);

        void DeleteLike(Like like);
    }
}
