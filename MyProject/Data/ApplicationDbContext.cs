﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MyProject.Models;

namespace MyProject.Data
{
    public class FinalProjectDBContext : IdentityDbContext<ApplicationUser>
    {
        public FinalProjectDBContext(DbContextOptions<FinalProjectDBContext> options)
            : base(options)
        {
        }
        public DbSet<Note> Notes { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<Like> Likes { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Note>()
                .HasOne(p => p.ApplicationUser)
                .WithMany(t => t.Notes)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Comment>()
                .HasOne(p => p.Note)
                .WithMany(t => t.Comments)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Comment>()
                .HasOne(p => p.ApplicationUser)
                .WithMany(t => t.Comments)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Like>()
                .HasKey(t => new { t.CommentId, t.ApplicationUserId });
        }
    }
}
