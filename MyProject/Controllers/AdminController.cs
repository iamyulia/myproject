﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using MyProject.Models;
using Microsoft.AspNetCore.Authorization;

namespace MyProject.Controllers
{
    [Authorize(Roles = "admin")]
    public class AdminController : Controller
    {
        private IRepository repository;

        private UserManager<ApplicationUser> userManager;

        private RoleManager<IdentityRole> roleManager;

        public AdminController(UserManager<ApplicationUser> usrMgr, RoleManager<IdentityRole> rlMgr, IRepository repo)
        {
            userManager = usrMgr;
            roleManager = rlMgr;
            repository = repo;
        }

        public async Task<ViewResult> Index()
        {
            List<ApplicationUser> admin = new List<ApplicationUser>();
            List<ApplicationUser> notAdmin = new List<ApplicationUser>();
            foreach(ApplicationUser user in userManager.Users)
            {
                var list = await userManager.IsInRoleAsync(user, "admin") ? admin : notAdmin;
                list.Add(user);
            }
            return View("View",new RoleModel
            {
                Admin= admin,
                NotAdmin = notAdmin
            });
        }

        [HttpPost]
        public async Task<IActionResult> ChangeStatus(string Id)
        {
            ApplicationUser user = await userManager.FindByIdAsync(Id);
            if (user != null)
            {                
                user.UserIsLocked = user.UserIsLocked ? false:true;
               await userManager.UpdateAsync(user);
            }
            else
            {
               ModelState.AddModelError("", "User Not Found");
            }

            return PartialView("_UserPartial", user);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
            ApplicationUser user = await userManager.FindByIdAsync(id);
            if (user != null)
            {
                repository.DeleteCommentsOfUser(id);
                IdentityResult result = await userManager.DeleteAsync(user);
                if (result.Succeeded)
                { 
                    AddErrorsFromResult(result);
                }
            }
            else
            {
                ModelState.AddModelError("", "User Not Found");
            }
            return PartialView("_MessagePartial","Пользователь удален");
        }

        private void AddErrorsFromResult(IdentityResult result)
        {
            foreach (IdentityError error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }
        }
        
        [HttpPost]
        public async Task<IActionResult> MakeAdmin(string id)
        {
            ApplicationUser user = await userManager.FindByIdAsync(id);
            if (user != null)
            {
                await userManager.AddToRoleAsync(user, "admin");
            }
            else
            {
                ModelState.AddModelError("", "User Not Found");
            }
            return PartialView("_AdminPartial",user);
        }

        [HttpPost]
        public async Task<IActionResult> RemoveFromAdmin(string id)
        {
            ApplicationUser user = await userManager.FindByIdAsync(id);
            if (user != null)
            {
                await userManager.RemoveFromRoleAsync(user, "admin");
            }
            else
            {
                ModelState.AddModelError("", "User Not Found");
            }
            return PartialView("_UserPartial", user);
        }
    }
}