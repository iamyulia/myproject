﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MyProject.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace MyProject.Controllers
{
    public class CommentController : Controller
    {
        private IRepository repository;

        private UserManager<ApplicationUser> userManager;

        public CommentController(IRepository repo, UserManager<ApplicationUser> usrMgr)
        {
            repository = repo;
            userManager = usrMgr;
        }

        public string GetCurrentUserId()
        {
            return User.FindFirstValue(ClaimTypes.NameIdentifier);
        }

        [HttpPost]
        public async Task<ActionResult> AddComment(Comment comment)
        {
            if (ModelState.IsValid)
            {
                comment.ApplicationUser = await userManager.FindByIdAsync(GetCurrentUserId());
                repository.AddComment(comment);
                return PartialView("_CommentPartial", comment);
            }
            else return PartialView("_MessagePartial", "Введите текст комментария");
            
        }

        [HttpPost]
        public bool DidLike(int commentId)
        {
            if (FoundLike(commentId) == null) { return false; }
            else { return true; }
        }

        [HttpPost]
        public int LikeCount(int commentId)
        {
            return repository.Likes.Where(p => p.CommentId == commentId).Count();
        }

        [HttpPost]
        public bool ChangeLike(int commentId)
        {
            Like like = FoundLike(commentId);
            if (like == null)
            {
                repository.AddLike(new Like { ApplicationUserId = GetCurrentUserId(), CommentId = commentId });
                return true;
            }
            else
            {
                repository.DeleteLike(like);
                return false;
            }
        }

        private Like FoundLike(int commentId)
        {
            var usersLikes = repository.Likes.Where(p => p.ApplicationUserId == GetCurrentUserId());
            return usersLikes.FirstOrDefault(p => p.CommentId == commentId);
        }

        [HttpPost]
        public IActionResult Delete(int commentId)
        {
            repository.DeleteComment(commentId);
            return PartialView("_MessagePartial", "Комментарий удален");
        }
    }
}