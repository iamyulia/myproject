﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyProject.Models;
using Microsoft.AspNetCore.Mvc;
using MyProject.Models.ViewModels;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace MyProject.Controllers
{
    public class NoteController : Controller
    {
        private IRepository repository;

        private UserManager<ApplicationUser> userManager;

        public int PageSize = 5;

        public NoteController(IRepository repo, UserManager<ApplicationUser> usrMgr)
        {
            repository = repo;
            userManager = usrMgr;
        }

        public ViewResult List(int notePage = 1)
        {
            return View(new NoteListViewModel
            {
                Notes = repository.Notes
                  .Include(p => p.ApplicationUser)
                  .OrderByDescending(p => p.Date)
                  .Skip((notePage - 1) * PageSize)
                  .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = notePage,
                    ItemsPerPage = PageSize,
                    TotalItems = repository.Notes.Count()
                }
            });
        }

        
        [Authorize]     
        [HttpGet]
        public ViewResult Create()
        {
            return View (new Note());
        }

        [HttpPost]
        public IActionResult Create(Note note)
        {
            if (ModelState.IsValid)
            {
                note.ApplicationUserId = GetCurrentUserId();
                note.Date = DateTime.Now.Date;
                repository.AddNote(note);
                TempData["message"] = $"{note.Name} создан";
                return RedirectToAction("List");
            }
            else
            {
                return View(note);
            }
        }

        [Authorize]
        public ViewResult Edit(int noteId)
        {
            return View(repository.Notes
            .FirstOrDefault(p => p.Id == noteId));
        }

        [Authorize]
        [HttpPost]
        public IActionResult Edit(Note note)
        {
            if (ModelState.IsValid)
            {
                repository.SaveNote(note);
                TempData["message"] = $"{note.Name} сохранен";
                return RedirectToAction("List");
            }
            else
            {
                return View(note);
            }
        }

        private string GetCurrentUserId()
        {
            return User.FindFirstValue(ClaimTypes.NameIdentifier);
        }
        
        private async Task<bool> CurrentUserIsAdmin()
        {
            var user = await userManager.GetUserAsync(User);
            if (user == null) return false;
            else return await userManager.IsInRoleAsync(user, "admin");
        }

        public async Task<ViewResult> Note(int id)
        {
            
            Note note = repository.Notes
                                  .Include(p => p.Comments)
                                    .ThenInclude(c => c.ApplicationUser)
                                  .Include(p => p.ApplicationUser)
                                  .Single(p => p.Id == id);
            
            ViewBag.IsAuthor = note.ApplicationUserId == GetCurrentUserId() ? true : false;
            if (await CurrentUserIsAdmin()) ViewBag.IsAuthor = true;
            return View(note);
        }
        
        [HttpPost]
        public IActionResult Delete(Note note)
        {
            repository.DeleteNote(note);
            TempData["Message"] = $"Конспект удален";
            return RedirectToAction("List");

        }
    }
}