﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyProject.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Korzh.EasyQuery.Linq;
using MyProject.Models;
using Microsoft.EntityFrameworkCore;

namespace MyProject.Controllers
{
    public class SearchController : Controller
    {
        private IRepository repository;

        public int PageSize = 5;

        public SearchController(IRepository repo)
        {
            repository = repo;
        }

        public ViewResult SearchList(string text, int notePage = 1)
        {
            IQueryable<Note> notes = repository.Notes;
            if (!string.IsNullOrEmpty(text))
            {
                notes = repository.Notes.FullTextSearchQuery(text);                   
            }
            
            return View(new NoteListViewModel
            {
                Notes = notes
                    .Include(p => p.ApplicationUser)
                    .OrderByDescending(p => p.Date)
                    .Skip((notePage - 1) * PageSize)
                    .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = notePage,
                    ItemsPerPage = PageSize,
                    TotalItems = notes.Count()
                }
            });
         
        }

       /* private IQueryable<Note> SearchNotes(string text)
        {
            IQueryable<Note> notes;
            if (!string.IsNullOrEmpty(text))
            {
                notes = repository.Notes.FullTextSearchQuery(text);
            }
            else
            {
                notes = repository.Notes;
            }
        }

        private IQueryable<Comment> SearchComment(string text)
        {

        }*/
    }
}